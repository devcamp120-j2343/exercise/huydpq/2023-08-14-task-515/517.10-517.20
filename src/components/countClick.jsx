import { Component } from "react";

class CountClick extends Component {
    constructor(props){
        super(props)
        this.state = {
            count: 0
        }
    }

    onBtnClickMe = () => {
        this.setState ({
            count: this.state.count + 1
        })
    }
    render(){
        return (
            <div>
                <button onClick={this.onBtnClickMe}>Click me</button>
                <p>Count {this.state.count} </p>
            </div>
        )
    }
}

export default CountClick