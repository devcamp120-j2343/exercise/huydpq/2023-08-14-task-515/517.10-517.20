import { Component } from "react";

class Info extends Component {
    
    render(){
        console.log(this.props)
        const {firstname, lastname, favNumber} = this.props
        return (
            
            <div>
                <p>My name is {lastname} {firstname} and my favourite number is {favNumber}</p>
            </div>
        )
    }
}

export default Info