
import Info from './components/componentInfo';
import CountClick from './components/countClick';

function App() {
  return (
    <div style={{margin: '30px'}}>
      <div>
        <p>517.10 Props</p>
        <Info firstname="Huy" lastname= "do" favNumber={30}/>

        <p>517.20 State</p>
        <CountClick/>
      </div>
    </div>
  );
}

export default App;
